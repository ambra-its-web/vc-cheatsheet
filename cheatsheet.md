# <div style="text-align: center">My Version Control Cheatsheet</div>

<div style="text-align: center; font-size:0.8em; color: pink">smol lil custom links legend</div>

### 1. [Initializing repos (**init**)](#init)

   ### <div style="text-align: right"> 2. [Cloning repos (**clone**)](#clone) </div>

### 3. [Adding changes (**add**)](#add)

   ### <div style="text-align: right"> 4. [Commit changes (**commit**)](#commit) </div>

### 5. [Status stuff](#status)

   ### <div style="text-align: right"> 6. [Log stuff](#log) </div>

### 7. [Branch stuff](#branch)

   ### <div style="text-align: right"> 8. [Checkout stuff](#checkout) </div>

### 9. [Merging branches](#merge)

   ### <div style="text-align: right"> 10. [Pushing to remote](#push) </div>

### 11. [Pull and fetch](#pull)

   ### <div style="text-align: right"> 12. [Adding remote to pre-existing repo](#remote) </div>

### 13. [View differences](#diff)

   ### <div style="text-align: right"> 14. [Resetting](#reset) </div>

### 15. [Reverting](#revert)

   ### <div style="text-align: right"> 16. [Rebasing](#rebase) </div>

### 17. [Cherry-picking from other commits](#cherry)

### <div style="text-align: right"> 18. [Blaming code lines](#blame) </div>

### 19. [Forking](#fork)

### <div style="text-align: right"> 20. [Tagging commits](#tag) </div>

### 21. [Config](#fork)

<sub><sup><div style="text-align: center; font-size:0.9em; color: #fff9d4">This file looks way cooler when read through a .md editor that supports markup tags</div></sup></sub>


## Commands
<div style="text-align: center; color: pink">Commands and what they do</div>

 ### 1. `git init` <a id="init"></a>  

    Initializes a new Git repo in the current dir, that lil' hidden .git dir

 - Specify `<dir_name>` and it'll either create it or init in there

 - `--initial-branch` to pick main branch name


 ### 2. `git clone <repo_url>` <a id="clone"></a> 

    Clones a remote repo in your current dir. The repo will be in a dir named after it
    
 - Add `<clone_name>` to pick the name of the clone

 ### 3. `git add <file>` <a id="add"></a> 
 
    Adds a file to the staging area

 - Use `.` to add all changed files. Never used anything else
 - Add `-p` to 
   - display a preview diff of the changes made: <div style="color: red; text-decoration: underline; display: inline">removed</div> and <div style="color: green; text-decoration: underline; display: inline">replacing</div>, <div style="color: white; text-decoration: underline; display: inline">new</div> 
   - decide how to treat each change with the hunk staging options

 ### 4. `git commit -m "<commit_msg>"` <a id="commit"></a> 
 
    Commits the changes in the staging area with a msg

 - Omitting `-m` will bring up a commit msg file to fill in in an editor
    - Leaving an empty msg aborts the commit

 - Add `-a` to skip the add changes step
 - Add `--amend` to modify the most recent commit msg 
 - If it's not the most recent, [rebase](#rebase) instead
 - if it's a pushed commit, you'll need to [force push](#force) to remote

 ### 5. `git status` <a id="status"></a> 
 
    Shows the status of the working dir and staging area like added files, files to add...

 - Sometimes gives useful command tips too when issues arise 
 
 - Add `--short` or `-s` for symbol info (like <span style="color: #de2f46">M</span> = modified)
 - Add `--branch` or `-b` to target a specific branch

 ### 6. `git log` <a id="log"></a> 
 
    Shows the commit history of the repo, with IDs, branches, head position...

 - Add `--oneline` or `-o` to omit date and author info

 - Add `--decorate` to view more info
 - Add `-a` to view all commits from all branches
   <a id="log-tag"></a> 
 - Add `--tags` to view commit tags, then <tag-name> to only show the commits with that tag
   -  [tag section](#tag)
 - You can bypass reading IDs with `HEAD~`, each tilde representing HEAD-1commit
 - Use `show` instead if you just need to check one commit's info
 
 ### 7. `git branch` <a id="branch"></a> 
 
    Lists all the branches in the local repo

 - Add `-a` to see both local and remote branches

 - Add `<branch_name>` to create a branch
 - Add `-d` or `-D` + `<branch_name>` to delete that branch
 - Add `-m <old_name> <new_name>` to rename

 ### 8. `git checkout <branch_name>` <a id="checkout"></a> 
 
    Switches to the specified branch

 - Add `-b` before <branch_name> to also create it on the fly

 - Has a variety of extra purposes:
    - add `.` instead to retrieve all deleted files staged, so after you've added

    - add `<file_name>` instead to retrieve files not yet added to staging.<br>
    ___**[NOTE]:**___ use `git reset <?optional commit> <file_path>` to remove the file deletion from the staging area after having brought back the file, or it will still be a part of the commit. Git assumes HEAD, so last commit, if the commit parameter is omitted
      - can also use `restore`

 ### 9. `git merge <branch_name>` <a id="merge"></a> 
 
    Merges current branch into the specified one
   
 - main source of nightmares

 ### 10. `git push <remote_branch> <branch_name>` <a id="push"></a> 
 
    Pushes the local changes to the remote repo

 - Specifying where and what is optional, but needs to happen once at least, then it's useful if you don't push directly on main or can't do that at all for lack of perms

 - Add `--set-upstream` or `-u` + `<remote_branch> <branch_name>` to set up the push upstream for that branch, so that you can later just use `git push` without specifying branches
 <a id="force"></a>
 - Add`-f` or `--force` if you need to replace the remote commit history with yours (i.e after rebasing). Dangerous since it overwrites history and may remove changes made by others

 ### 11. `git pull origin <branch_name>` <a id="pull"></a> 

    Pulls the remote changes from the remote repo with a specified branch
 
 - Use `fetch` instead to just get the changes without committing just yet
 - Omit all to just pull all from main

 ### 12. `git remote add <remote_name> <repo_url>` <a id="remote"></a>
 
    Adds a remote repo with the specified name and URL

 - Do `remote -v` to show all remote repos associated with the local one

 ### 13. `git diff`  <a id="diff"></a>
 
    Shows differences between working dir and staging area

 - Add `--staged`	to show only those between staging area and last commit

 - add `<commit_id>` to show those between working dir and that commit, or add more to differentiate those instead
 - Add `branchB...branchA` to show the diff in changes in that commit range

 ### 14. `git reset`  <a id="reset"></a>

    Moves HEAD to the last commit and unstages current changes

 - Dangerous without params. If you use it and regret it, use `git reflog` to view HEAD movement logs and reset again specifying the commit id where prev changes were

 - Add `<file_name>` to remove it from staging area
 - Add `<commit_id>` to specify which commit to move the HEAD to

 Use the following preferably with the <commit_id> param
 - Add `--soft` to <span style="color: green; font-weight:1000">keep the staged changes</span>
    - Useful if you want more than one commit to become one
 - Add `--mixed` to <span style="color: orange; font-weight:1000">unset staged changes but keep them in the working dir</span>. Default one
    -  Useful if you want to set stages differently and commit again with diff order

 - Add `--hard` to <span style="color: red; font-weight:1000">fully discard</span> changes.
    - Useful if you want multiple commits on a different branch:
        - make branch, go back to prev branch, hard reset, now work with changes only on new branch

 ### 15. `git revert <commit_id>`  <a id="revert"></a>

    Creates a new commit that undoes the changes made in the specified one
 - Useful if you need the changes to still be visible in the history, since it doesn't discard them like reset
 - Add `..` between <commit_id>s to specify a range 

 ### 16. `git rebase`  <a id="rebase"></a>

    Combines all commits after the current branch's diversion and moves the new commit branch to the new HEAD position, the last remote commit

                                                       Basically:
                                             1  2  3                          1  2  3
                                             _____!      rebase               ______!
                                            /             --->               / 
                                       ----<-----o                 ---------o

 - Useful if you're behind on remote commits but have already made changes to keep, or if you actually need those new commits for your current changes
 - Also helps prevent conflicts
    
 - Error with no params if there's no remote
 
 - Add `<branch_name> `to specify what branch you want to rebase to, even local

 ### 17. `git cherry-pick <commit_id>`  <a id="cherry"></a>

    Commits the changes made in a specific commit to the current branch.

 - Useful for applying changes from one branch to another, or for applying changes from a past commit to the current branch.
 - Add `-n` to specify not to commit and just add to staging for review and manual commit

 - Since it autocommits without -n, you can add -e and -s like a regular commit:
   
   - add `-e` to edit the commit comment file

   - add `-s` to sign it off

   - add `-x` if the prev commit comment was good

 ### 18. `git blame <file_name`  <a id="blame"></a>

    Shows commit ID, author, date, and time of last modification for each file line

 - Add `-L <starting-line>, <ending-line>` to display blame for that range of lines in the file

 - Add `-C` to also find lines moved or copied from other parts of the **same file**, useful for debugging or understanding file history   
 - Add `-M` to also find lines moved or copied from **other files**
 - Add `-w` to ignore whitespace changes, useful to remove clutter

 ### 19. Forking  <a id="revert"></a>

    Creating an independent cloned repo with you as owner

 - Allows for more control

 - Start new projects from existing ones
 - Can make a pull requests on Gitlab to merge your fork or its specific branches to the original project
   - through merge then push in bash or by requesting

 ### 20. `git tag <tag_name>`  <a id="tag"></a>

    Marks a commit of the repo history

 - Like branches, they point to a commit

 - No params to view all tags in the repo history
 - Used to mark important things like versions or category bookmarks
 - Add `-a` before <tag_name> to make it an annotated one, and give it a comment adding `-m <msg>`
 - Add `show` before <tag_name> to view its info
 - Can be used with the log command
   - [log section](#log-tag)

 ### 21.
 ### `git config --global user.email <email>`
 ### `git config --global user.name <username>`

    Configures your sign thingies for commits globally for repos

 - Technically first thing to do but I remembered too late
 - Modify `.gitignore` file to auto omit files from staging